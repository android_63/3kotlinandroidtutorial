package buu.example.a3kotlinandroidtutorial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast

class ListViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)
        val arrayAdapter: ArrayAdapter<*>
        val users = arrayOf(
            "Virat Kohki", "Rohit Shama", "Steve Smith", "Kane Willimson", "Ross Taylor",
            "Virat Kohki", "Rohit Shama", "Steve Smith", "Kane Willimson", "Ross Taylor",
            "Virat Kohki", "Rohit Shama", "Steve Smith", "Kane Willimson", "Ross Taylor",
            "Virat Kohki", "Rohit Shama", "Steve Smith", "Kane Willimson", "Ross Taylor"
        )
        val userList =findViewById<ListView>(R.id.userList)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, users)
        userList.adapter = arrayAdapter
        userList.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this@ListViewActivity, "" + users[position], Toast.LENGTH_LONG).show()
            val intent = Intent(this@ListViewActivity, ShowNameActivity::class.java)
            intent.putExtra("name", users[position])
            startActivity(intent)
        }
    }
}